# DropLab

A generic dropdown for all of your custom dropdown needs.

## Install

* Add [`/dist/droplab.js`](/dist/droplab.js) to your application assets.
* Link to them in your code. See [examples](#examples).

## Usage

Adding the `data-dropdown` attribute to your dropdown will add a droplab hook for that element.

The `id` of the hooked element can be used as the value of the `data-dropdown-trigger`
attribute of a different element to use it as a toggle.


```html
<a href="#" data-dropdown-trigger="#some-dropdown">Toggle</a>

<ul id="some-dropdown" data-dropdown>
  <!-- ... -->
<ul>
```

You can also have an `input` as the trigger. When a user types, the dropdown will appear.

### Static data

You can add static list items.

```html
<a href="#" data-dropdown-trigger="#static-dropdown">Toggle</a>

<ul id="static-dropdown" data-dropdown>
  <li><a href="#">Jacob</a></li>
  <li><a href="#">Jeff</a></li>
  <!--  ... -->
</ul>
```

### Dynamic data

Adding `data-dynamic` to your dropdown element will enable dynamic list rendering.

Data can be added using the `DropLab.prototype.addData` function. The first argument is the `data-id` of your trigger element and the second argument is an array of objects to render to list items.

You can template a list item using the keys of the data object provided.

```html
<a href="#" data-dropdown-trigger="#dynamic-dropdown" data-id="dynamic-trigger">Toggle</a>

<ul id="dynamic-dropdown" data-dropdown data-dynamic>
  <li><a href="#" data-id="{{id}}">{{text}}</a></li>
</ul>
```

```js
droplab.addData('dynamic-trigger', [{
    id: 0,
    text: 'Jacob'
  }, {
    id: 1,
    text: 'Jeff'
  }]);
```

### Examples

Some example implementations are provided in [`/example`](example).

## Plugins

Plugins are functions that are registered to be executed when a droplab instance is initialised.

They are passed the `DropLab` function.

```js
droplab.plugin(function somePluginFunction(DropLab) {
  // ...
});
```

Some plugins are available in [`/dist/plugins`](/dist/plugins).

### Plugin documentation

* [ajax.datasource](/docs/plugins/ajax.datasource.md)
* [filter](/docs/plugins/filter.md)

## Development

To run the tests, you'll need to first start your static server: `npm start`.

Then you can run tests with `npm test`.

Scripts can be compiled into [`/dist`](/dist) with `npm run compile`.
